package de.cherriz.training.rest.notiz.server;

import java.util.Optional;

public interface NotizService {

    Notiz create(Notiz liste);

    Optional<Notiz> find(Long id);

    Iterable<Notiz> findAll();

    void delete(Long id);

}