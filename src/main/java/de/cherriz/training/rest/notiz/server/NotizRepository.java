package de.cherriz.training.rest.notiz.server;

import org.springframework.data.repository.CrudRepository;

public interface NotizRepository extends CrudRepository<Notiz, Long> {

}
