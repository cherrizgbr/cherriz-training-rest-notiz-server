package de.cherriz.training.rest.notiz.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping(value = "/notiz")
public class NotizController {

    private final NotizService notizService;

    @Autowired
    public NotizController(final NotizService notizService) {
        this.notizService = notizService;
    }

    @RequestMapping(value = "/systemcheck", method = RequestMethod.GET)
    public SystemCheck systemcheck() {
        return new SystemCheck("EntityManager", this.notizService != null ? "OK" : "Error");
    }

    @RequestMapping(method = RequestMethod.POST)
    public Notiz create(@RequestBody @Valid Notiz notiz) {
        return notizService.create(notiz);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Notiz get(@PathVariable Long id) {
        Optional<Notiz> notiz = notizService.find(id);
        if (notiz.isPresent()) {
            return notiz.get();
        }
        throw new NoSuchElementException("Die angegebene Ressource existiert nicht.");
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Long> getAll() {
        return StreamSupport.stream(notizService.findAll().spliterator(), false).map(Notiz::getId).collect(Collectors.toList());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        notizService.delete(id);
    }

}