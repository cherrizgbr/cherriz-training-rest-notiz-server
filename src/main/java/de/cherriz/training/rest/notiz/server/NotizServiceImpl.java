package de.cherriz.training.rest.notiz.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.Optional;

@Service
@Validated
public class NotizServiceImpl implements NotizService {

    private final NotizRepository repository;

    @Autowired
    public NotizServiceImpl(final NotizRepository repository) {
        this.repository = repository;
    }

    @Override
    public Notiz create(Notiz liste) {
        return repository.save(liste);
    }

    @Override
    public Optional<Notiz> find(Long id) {
        return repository.findById(id);
    }

    @Override
    public Iterable<Notiz> findAll() {
        return repository.findAll();
    }

    @Override
    public void delete(Long id){
        if(repository.existsById(id)){
            repository.deleteById(id);
        }
    }

}