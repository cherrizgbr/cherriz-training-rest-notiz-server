package de.cherriz.training.rest.notiz.server;

import java.util.HashMap;
import java.util.Map;

public class SystemCheck {

    public static final String OK = "OK";

    public static final String ERROR = "ERROR";

    private final String status;

    private final Map<String, String> systems;

    public SystemCheck(String... systemsWithStatus) {
        String gesamtStatus = OK;
        this.systems = new HashMap<>();
        for (int i = 0; i + 1 < systemsWithStatus.length; i += 2) {
            String system = systemsWithStatus[i];
            String status = systemsWithStatus[i + 1];
            this.systems.put(system, status);
            if (!status.equals(OK)) {
                gesamtStatus = ERROR;
            }
        }
        this.status = gesamtStatus;
    }

    public String getStatus() {
        return status;
    }

    public Map<String, String> getSystems() {
        return systems;
    }

}